package org.example.interfaces;

import java.util.Date;
import java.util.List;

public interface IDAO<T> {

    //Exo1
    boolean create(T o);

    boolean update(T o);

    boolean delete(T o);

    //Exo2
    T findById(int id);
    List<T> findAll();

    List<T> filterByPrice(double min) throws Exception;

    List<T> filterByDate(Date dateMin, Date dateMax) throws Exception;

    //Exo3

    List<T> filterByStock(int max) throws Exception;

    //Exo4

    List<T> filterByMarque(String marque);
    List<T> valueStockByMarque(String marque);

    List<T> deleteByMarque(String marque);

    double moyennePrixProduits();

    List<T> menu(int choix);
}
