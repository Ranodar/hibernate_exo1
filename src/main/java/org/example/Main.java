package org.example;

import org.example.entities.Produit;
import org.example.services.ProduitService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
//        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
//        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
//        Session session = sessionFactory.openSession();


//        Produit p1 = new Produit();
//        Produit p2 = new Produit();
//        Produit p3 = new Produit();
//        Produit p4 = new Produit();
//        Produit p5 = new Produit();
//
//        p1.setMarque("apple");
//        p1.setReference("APPLE01");
//        p1.setPrix(50);
//        p1.setStock(100);
//        p2.setMarque("hp");
//        p2.setReference("HP02");
//        p2.setPrix(10);
//        p2.setStock(1000);
//        p3.setMarque("acer");
//        p3.setReference("acer03");
//        p3.setPrix(25);
//        p3.setStock(250);
//        p4.setMarque("lenovo");
//        p4.setReference("LENOVO04");
//        p4.setPrix(100);
//        p4.setStock(10);
//        p5.setMarque("msi");
//        p5.setReference("MSI05");
//        p5.setPrix(75);
//        p5.setStock(75);
//
//        session.save(p1);
//        session.save(p2);
//        session.save(p3);
//        session.save(p4);
//        session.save(p5);


//        session.getTransaction().begin();
//
//        //afficher produit avec id=2
//        Produit p_id2 = session.load(Produit.class, Long.valueOf(2));
//        System.out.println(p_id2.toString());
//
//        //supprimer produit avec id=3
//        Produit p_id3 = session.load(Produit.class, Long.valueOf(3));
//        session.delete(p_id3);
//
//        //modifier produit avec id=1
//        Produit p_id1 = session.load(Produit.class, Long.valueOf(2));
//        p_id1.setMarque("acer");
//        p_id1.setReference("ACER03");
//        p_id1.setPrix(10);
//        p_id1.setStock(5);
//        session.update(p_id1);
//
//        session.getTransaction().commit();
//        session.close();
//        sessionFactory.close();


        //via ProduitService implements IDAO

        //create produit
        ProduitService produitService = new ProduitService();
//        produitService.create(new Produit("SAMSUNG","SGS10",new Date("2022/01/01"),999,99));
//        produitService.create(new Produit("HP","EER678",new Date("2016/01/01"),99,990));
//        produitService.create(new Produit("SONY VAIO","AQWZSX",new Date("2014/01/01"),899,990));
//        produitService.create(new Produit("DELL","AZERTY",new Date("2020/01/01"),99,990));
//        produitService.create(new Produit("SONY","qsdERT",new Date("2012/01/01"),99,990));
//
//        //récupérer infos produit id=7
//        Produit p = produitService.findById(7);
//        System.out.println(p.getId()+" , "+p.getMarque()+" , "+p.getReference());
//
//        //supprimer produit avec id=8
//        produitService.delete(produitService.findById(8));
//
//        //modifier produit avec id=6
//        p = produitService.findById(6);
//        if (p != null){
//            p.setMarque("HP");
//            p.setReference("MMMMMMPPPPP");
//            p.setDateAchat(new Date("2015/09/08"));
//            p.setPrix(50000);
//            produitService.update(p);
//        }

        // tout récupérer
//        System.out.println("Exo2");
//        System.out.println();
//        System.out.println("1. Afficher tout les produits");
//        System.out.println();
//        Query<Produit> produitQuery = session.createQuery("from Produit");
//        List<Produit> produits = produitQuery.list();
//        for (Produit p : produits) {
//            System.out.println(p.getId()+" , "+p.getReference());
//        }
//        List<Produit> allProduits = produitService.findAll();
//        for (Produit p : allProduits) {
//            System.out.println(p.getId()+" , "+p.getReference());
//        }

        // récupérer produits prix supérieur à 100€
//        System.out.println();
//        System.out.println("2. Afficher tout les produits dont le prix est supérieur à 100€");
//        System.out.println();
//        Query<Produit> produitQuery2 = session.createQuery("from Produit where prix>100 ");
//        List<Produit> produits1 = produitQuery2.list();
//        for (Produit p : produits1) {
//            System.out.println(p.getId()+" , "+p.getReference()+" , "+ p.getPrix());
//        }

//        List<Produit> produitsPasCher = null;
//        try {
//            produitsPasCher = produitService.filterByPrice(100);
//            for (Produit p : produitsPasCher) {
//                System.out.println(+p.getPrix());
//            }
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }


        // récupérer produits entre deux dates
//        System.out.println();
//        System.out.println("3. Afficher la liste des produits entre 2 dates");
//        System.out.println();
//        Query<Produit> produitQuery3 = session.createQuery("from Produit where dateAchat between '2015/01/01' and '2021/01/01'");
//        List<Produit> produits2 = produitQuery3.list();
//        for (Produit p : produits2) {
//            System.out.println(p.getId()+" , "+p.getReference()+" , "+ p.getDateAchat());
//        }
//        String date1 = "2015/01/01";
//        String date2 = "2021/01/01";
//        Date dateMin = new SimpleDateFormat("yyyy/MM/dd").parse(date1);
//        Date dateMax = new SimpleDateFormat("yyyy/MM/dd").parse(date2);
//        List<Produit> produitsByDate = produitService.filterByDate(dateMin,dateMax);
//        for (Produit p : produitsByDate) {
//            System.out.println(p.getId()+" , "+p.getReference()+" , "+ p.getDateAchat());
//        }

        //Exo3

        //Afficher la liste des produits commandés entre deux dates lues au clavier
//        Scanner saisirEntree = new Scanner(System.in);
//        System.out.println("Date minimum : ");
//        String minDate = saisirEntree.nextLine();
//        Scanner saisirEntree2 = new Scanner(System.in);
//        System.out.println("Date maximum : ");
//        String maxDate = saisirEntree2.nextLine();
//        Date dateMin = new SimpleDateFormat("yyyy/mm/dd").parse(minDate);
//        Date dateMax = new SimpleDateFormat("yyyy/mm/dd").parse(maxDate);
//        try {
//            List<Produit> produitsByDate = produitService.filterByDate(dateMin,dateMax);
//            for (Produit p : produitsByDate) {
//                System.out.println(p.getId()+" , "+p.getReference()+" , "+ p.getDateAchat());
//            }
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

        //Retourner les numéros et références des articles dont le stock est inférieur à une valeur lue au clavier*
//        Scanner saisirEntree = new Scanner(System.in);
//        System.out.println("Le stock maximum :");
//        Integer max = saisirEntree.nextInt();
//        List<Produit> produitsEnStock = null;
//        try {
//            produitsEnStock = produitService.filterByStock(max);
//            for (Produit p : produitsEnStock) {
//                System.out.println(p.getId()+" , "+p.getReference()+" , "+p.getStock());
//            }
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

        //Exo4

        //1. valeur du stock total by marque
//        int valeurTotale = 0;
//        Scanner saisirEntree = new Scanner(System.in);
//        System.out.println("Choisir une marque :");
//        String marqueChoisie = saisirEntree.nextLine();
//        List<Produit> produitByMarque = null;
//        produitByMarque = produitService.valueStockByMarque(marqueChoisie);
//        for (Produit p : produitByMarque) {
//            valeurTotale += p.getStock()*p.getPrix();
//        }
//        System.out.println("Valeur totale du stock de la marque : "+valeurTotale);


        //2. moyenne prix des produits
//        double moyennePrixProduits = produitService.moyennePrixProduits();
//        System.out.println("Prix moyen des produits : "+moyennePrixProduits);

        //3. filterbyMarque
//        Scanner saisirEntree = new Scanner(System.in);
//        System.out.println("Choisir une marque :");
//        String marqueChoisie = saisirEntree.nextLine();
//        List<Produit> produitByMarque = null;
//        produitByMarque = produitService.filterByMarque(marqueChoisie);
//        for (Produit p : produitByMarque) {
//            System.out.println(p.getId()+" , "+p.getMarque());
//        }

        //4. delete by marque
//        Scanner saisirEntree2 = new Scanner(System.in);
//        System.out.println("Choisir une marque :");
//        String marqueChoisie = saisirEntree2.nextLine();
//        List<Produit> produitByMarque = null;
//        produitByMarque = produitService.deleteByMarque(marqueChoisie);


//        session.getTransaction().commit();
//        session.close();
//        sessionFactory.close();

    }
}