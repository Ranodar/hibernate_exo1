package org.example;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Pile<T> {
    private T[] elements;
    private int count;


    public Pile(int taille) {
        elements = (T[]) Array.newInstance(elements.getClass(), taille);
        count = 0;
    }

    public boolean Empiler(T element){
        if (count < elements.length){
            elements[++count] = element;
            return true;
        }
        return  false;
    }

    public boolean Depiler(){
        if (count > 0){
            elements[--count] = null;
            return true;
        }
        return  false;
    }

    public T GetElement(int id) throws Exception {
        if (id >= 0 && id < elements.length && elements[id] != null) {
            return elements[id];
        }
        throw new Exception("Not found");
    }
}
