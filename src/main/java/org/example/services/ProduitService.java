package org.example.services;


import org.example.entities.Produit;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ProduitService implements IDAO<Produit> {

    private StandardServiceRegistry registry;
    private SessionFactory sessionFactory;

    public ProduitService(){
        registry = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }
    @Override
    public boolean create(Produit o) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Produit o) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Produit o) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Produit findById(int id) {
        Produit produit = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        produit = session.get(Produit.class,id);
        session.getTransaction().commit();
        return produit;
    }

    @Override
    public List<Produit> findAll() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery = session.createQuery("from Produit");
        return produitQuery.list();
    }

    @Override
    public List<Produit> filterByPrice(double min) throws Exception {
        if (min>0){
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Query<Produit> produitQuery1 = session.createQuery("from Produit where prix>:min");
            produitQuery1.setParameter("min",min);
            session.getTransaction().commit();
            return produitQuery1.list();
        }
        throw new Exception("error value");
    }

    @Override
    public List<Produit> filterByDate(Date dateMin, Date dateMax) throws Exception {
        if (dateMin.before(dateMax)){
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            Query<Produit> produitQuery2 = session.createQuery("from Produit where dateAchat between ':dateMin' and 'dateMax'");
            produitQuery2.setParameter("dateMin",dateMin);
            produitQuery2.setParameter("dateMax",dateMax);
            session.getTransaction().commit();
            return produitQuery2.list();
        }
        throw new Exception("error date");


    }

    @Override
    public List<Produit> filterByStock(int max) throws Exception {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery4 = session.createQuery("from Produit where stock<:max");
        produitQuery4.setParameter("max",max);
        session.getTransaction().commit();
        return produitQuery4.list();
    }

    @Override
    public List<Produit> filterByMarque(String marque){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery1 = session.createQuery("from Produit where marque like :marque");
        produitQuery1.setParameter("marque",marque);
        session.getTransaction().commit();
        return produitQuery1.list();
    }

    @Override
    public List<Produit> valueStockByMarque(String marque) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery1 = session.createQuery("from Produit where marque like :marque");
        produitQuery1.setParameter("marque",marque);
        session.getTransaction().commit();
        return produitQuery1.list();
    }

    @Override
    public List<Produit> deleteByMarque(String marque) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Query<Produit> produitQuery1 = session.createQuery("delete from Produit where marque like :marque");
        produitQuery1.setParameter("marque",marque);

        produitQuery1.executeUpdate();
        session.getTransaction().commit();
        return produitQuery1.list();
    }

    @Override
    public double moyennePrixProduits() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        double produitQuery = (double) session.createQuery("select avg(prix) from Produit").uniqueResult();

        return produitQuery;
    }

    @Override
    public List<Produit> menu(int choix) {
        ProduitService produitService = new ProduitService();
        System.out.println("Menu");
        System.out.println("");
        System.out.println("1. Stock par marque");
        System.out.println("2. Calculer prix moyen des produits");
        System.out.println("3. Filtrer par marque");
        System.out.println("4. Delete par marque");
        System.out.println("5. Ajouter un produit");
        System.out.println("6. Sortir");

        switch (choix){
            case 1: int valeurTotale = 0;
        Scanner saisirEntree = new Scanner(System.in);
        System.out.println("Choisir une marque :");
        String marqueChoisie = saisirEntree.nextLine();
        List<Produit> produitByMarque = null;
        produitByMarque = produitService.valueStockByMarque(marqueChoisie);
        for (Produit p : produitByMarque) {
            valeurTotale += p.getStock()*p.getPrix();
        }
        System.out.println("Valeur totale du stock de la marque : "+valeurTotale);
        ;
                break;
            case 2: double moyennePrixProduits = produitService.moyennePrixProduits();
        System.out.println("Prix moyen des produits : "+moyennePrixProduits);
                ;
                break;
            case 3: Scanner saisirEntree2 = new Scanner(System.in);
        System.out.println("Choisir une marque :");
        String marqueChoisie2 = saisirEntree2.nextLine();
        List<Produit> produitByMarque2 = null;
        produitByMarque2 = produitService.filterByMarque(marqueChoisie2);
        for (Produit p : produitByMarque2) {
            System.out.println(p.getId()+" , "+p.getMarque());
        }
                ;
                break;
            case 4: Scanner saisirEntree3 = new Scanner(System.in);
        System.out.println("Choisir une marque :");
        String marqueChoisi3 = saisirEntree3.nextLine();
        List<Produit> produitByMarque3 = null;
        produitByMarque3 = produitService.deleteByMarque(marqueChoisi3);
                ;
                break;
            case 5:

                ;
                break;
            case 6:

                ;
                break;
            default:
                System.out.println("Veuillez faire un choix ou sortir");
                break;
        }
        return menu(choix);
    }
}
